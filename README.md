 SMEFT combination exercise

Repository for the SMEFT combination exercise in the LHC EFT WG

[[_TOC_]]

## Conventions

### Propsed format for inputs
yaml file with entries `measurement` (e.g. EXPERIMENT_PROCESS_OBSERVABLE_insINSPIREID), `covariance`, `measured`, `sm`, and, optionally, `names` (bin boundaries, STXS bin, precision observable name...)

Can be created, for example, from python with PyYAML
```
import yaml

cov = [[1.,0.],
       [0.,1.]]
meas = [1.,1.]
sm = [1.,1.]

out={'covariance':cov,
     'measured':meas,
     'sm':sm}

with open('test.yaml', 'w') as f:
    data = yaml.dump(out, f)
```

### Sensitivity Study for determining fit directions

Typically in EFT fits that are currently performed, not all parameters can be simultaneously constrained due to 
lack of information. A sensitivity study is performed by propagating the expected HESSE matrix of the measurements to 
the SMEFT space by using the linear parameterisation,

To prepare the combined information matrix the script `prepare_combined_info.py` can be used. An example usage is shown 
below where the different inputs are passed using the `--input` flag such as `--input tag parameterisation.yaml covariance.yaml`.  
```
python3 scripts/prepare_combined_info.py \
       --input Higgs parameterisation/Higgs/param_Higgs.yaml inputs/Higgs/Higgs_STXSxBR_mu_exp.yaml \
       --input WW parameterisation/WW/param_WW.yaml inputs/WW/WW.yaml \
       --input WZ parameterisation/WZ/param_WZ.yaml inputs/WZ/WZ.yaml \
       --input Zjj parameterisation/Zjj/param_Zjj.yaml inputs/Zjj/Zjj.yaml \
       --input Zpole parameterisation/Zpole/param_Zpole.yaml inputs/Zpole/Zpole.yaml \
       --output run/info.yaml
```
Information matrices for different combination scenario can be constructed using the above script by providing different set of inputs in the `--input` flags. 

Once the information matrix is prepared, the rotation can be performed by using the script `get_sensitive_directions.py` to perform the PCA rotation within the multiple groups given by `--group`. To perform a full EV decomposition all the parameters can be given together in a single `--group` flag. Here is the command where different parameters are grouped by different physics impact.
```
python3 senstiEFT/scripts/get_sensitive_directions.py --input run/info.json \
       --group CHG --group CTGRE --group CTHRE \
       --group name=C_H_NORM CHBOX CDHRE CUHRE CEHRE --group CW --group CBHRE \
       --group name=CTOP CG CQD1 CQD8 CQJ11 CQJ18 CQJ31 CQJ38 CQU1 CQU8 CTJ1 CTJ8 CTU1 CTU8 \
       --group name=C_LEP_EW CHQ1 CHB CHDD CHQ3 CHW CHWB CHD CHE CHJ1 CHJ3 CHL1 CHL3 CHT CHU CLL1 CTBRE CTWRE CHBQ \
       --group name=C4Q CDD8 CUD8 CJJ38 CUD1 CJJ18 CJU1 CUU8 CUU1 CJJ11 CDD1 CJD8 CJD1 CJU8 CJJ31 \
       --group name=C2Q2L CLD CLJ3 CLU CJE CLJ1 \
       --group-name globalEFT_subgroup --output run/rotmat.yaml
```
It is desirable to edit the `ypars` in the `run/rotmat.json` to make the names of the rotated parameters shorter.
Before running any sort of fit the sensitivity study can also be used to derive the expected errors for the fit paramaters. This is done 
with the `estimate_covariance.py` script,
```
python3 senstiEFT/scripts/estimate_covariance.py \
       --info-matrix run/info.json \
       --transformation-matrix run/rotmat.yaml \
       --pois C_LEP_EW_01 C_LEP_EW_02 C_LEP_EW_03 C_LEP_EW_04 C_LEP_EW_05 C_LEP_EW_06 C_LEP_EW_07 \
       C_LEP_EW_08 C_LEP_EW_09 C_LEP_EW_10 C_LEP_EW_11 C_LEP_EW_12 C_LEP_EW_13 C_LEP_EW_14 \
       CW CBHRE C_H_NORM_01 C4Q_01 C2Q2L_01 CHG CTHRE CTGRE CTOP_01
```

To save the expected correlation from the sensitivity study, provide to the above script the flag `--save-correlation <out.pdf>` 
Once edited this rotation matrix can be used to but the rotated workspace using the script `prepare_combined_workspace.py`. 


```
python3 scripts/prepare_combined_workspace.py \
       --input Higgs parameterisation/Higgs/param_Higgs.yaml inputs/Higgs/Higgs_STXSxBR_mu_obs.yaml \
       --input WW parameterisation/WW/param_WW.yaml inputs/WW/WW.yaml \
       --input WZ parameterisation/WZ/param_WZ.yaml inputs/WZ/WZ.yaml \
       --input Zjj parameterisation/Zjj/param_Zjj.yaml inputs/Zjj/Zjj.yaml \
       --input Zpole parameterisation/Zpole/param_Zpole.yaml inputs/Zpole/Zpole.yaml \
       --rotation run/rotmat.yaml --output combined_workspace_rotated.root
``` 
It is also desirable to have the workspace in the warsaw basis, this can be done by simply leaving out the `--rotation` flag as follows,
```
python3 scripts/prepare_combined_workspace.py \
       --input Higgs parameterisation/Higgs/param_Higgs.yaml inputs/Higgs/Higgs_STXSxBR_mu_obs.yaml \
       --input WW parameterisation/WW/param_WW.yaml inputs/WW/WW.yaml \
       --input WZ parameterisation/WZ/param_WZ.yaml inputs/WZ/WZ.yaml \
       --input Zjj parameterisation/Zjj/param_Zjj.yaml inputs/Zjj/Zjj.yaml \
       --input Zpole parameterisation/Zpole/param_Zpole.yaml inputs/Zpole/Zpole.yaml \
       --output combined_workspace_warsaw.root
``` 


## Higgs

### Hyy, HZZ, and VHbb STXS

* Observable: merged simplified template cross-section bins from Hyy, HZZ, and VHbb
* Rivet: https://gitlab.cern.ch/LHCHIGGSXS/LHCHXSWG2/STXS/Classification
* Measurement and SM: [Higgs_STXSxBR_mu.yaml](inputs/Higgs/Higgs_STXSxBR_mu.yaml) 
* Reference: [ATLAS-CONF-2020-053](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2020-053/)

## SM EW 

### WW

* Observable: leading lepton pT
* Rivet: https://rivet.hepforge.org/analyses/ATLAS_2019_I1734263
* Measurement and SM: [WW.yaml](inputs/WW/WW.yaml)
* MG cards: [proc_card_mg5.dat](inputs/WW/proc_card_mg5.dat), [run_card.dat](inputs/WW/run_card.dat), [param_card.dat](inputs/WW/param_card.dat)
* Notes: Sherpa SM prediction used
* Reference: [Eur. Phys. J. C 79 (2019) 884](https://link.springer.com/article/10.1140/epjc/s10052-019-7371-6?wt_mc=Internal.Event.1.SEM.ArticleAuthorIncrementalIssue&utm_source=ArticleAuthorIncrementalIssue&utm_medium=email&utm_content=AA_en_06082018&ArticleAuthorIncrementalIssue_20191102)

### WZ

* Observable: mT WZ
* Rivet: *blacklisted* -- need to find the most appropriate ~public version
* Measurement and SM: [WZ.yaml](inputs/WZ/WZ.yaml)
* MG cards: [proc_card_mg5.dat](inputs/WZ/proc_card_mg5.dat), [run_card.dat](inputs/WZ/run_card.dat), [param_card.dat](inputs/WZ/param_card.dat)
* Notes: Sherpa SM prediction
* Reference: [Eur. Phys. J. C 79 (2019) 535](https://link.springer.com/article/10.1140/epjc/s10052-019-7027-6)

### EW Zjj

* Observable: delta phi jj
* Rivet: https://rivet.hepforge.org/analyses/ATLAS_2020_I1803608
* Measurement and SM: [Zjj.yaml](inputs/Zjj/Zjj.yaml)
* MG cards: [proc_card_mg5.dat](inputs/Zjj/proc_card_mg5.dat), [run_card.dat](inputs/Zjj/run_card.dat), [param_card.dat](inputs/Zjj/param_card.dat)
* Notes: VBFNLO+Herwig SM prediction 
* Reference: [Eur. Phys. J. C 81 (2021) 163](https://link.springer.com/article/10.1140/epjc/s10052-020-08734-w)


## EWPD

### Zpole data
* Rivet: N/A
* Measurement and SM: [Zpole.yaml](inputs/Zpole/Zpole.yaml)
* Reference:

  *data*
  * SLD Electroweak Group, DELPHI, ALEPH, SLD, SLD Heavy Flavour Group,
  OPAL, LEP Electroweak Working Group, L3 collaboration, Precision electroweak
  measurements on the Z resonance, Phys. Rept. 427 (2006) 257 [hep-ex/0509008].
  
  *Predictions*
  
  * M. Awramik, M. Czakon and A. Freitas, Electroweak two-loop corrections to the effective weak
  mixing angle, JHEP 11 (2006) 048 [hep-ph/0608099].
  
  * I. Dubovyk, A. Freitas, J. Gluza, T. Riemann and J. Usovitsch, Electroweak pseudo-observables
  and Z-boson form factors at two-loop accuracy, JHEP 08 (2019) 113 [1906.08815].
  
  * A. Freitas, Higher-order electroweak corrections to the partial widths and branching ratios of the
  Z boson, JHEP 1404 (2014) 070 [1401.2447].
  
  * M. Awramik, M. Czakon, A. Freitas and G. Weiglein, Precise prediction for the W boson mass in the standard model, Phys.Rev. D69 (2004) 053006 [hep-ph/0311148].
  
  *Input parameters*
  
  * Particle Data Group collaboration, Review of Particle Physics, PTEP 2020 (2020) 083C01.
  
  * CDF, D0 collaboration, Combination of CDF and D0 W -Boson Mass Measurements, Phys.
  Rev. D88 (2013) 052018 [1307.7627].
  
  * Particle Data Group collaboration, Review of Particle Physics, Chin. Phys. C40 (2016) 100001.
  
  * P. J. Mohr, B. N. Taylor and D. B. Newell, CODATA Recommended Values of the Fundamental Physical Constants: 2010, Rev. Mod. Phys. 84 (2012) 1527 [1203.5425].
  
  *Parametrization*
  
  * T. Corbett, A. Helset, A. Martin and M. Trott, EWPD in the SMEFT to dimension eight, JHEP 06 (2021) 076, [2102.02819].
