#from evpar import evs,fullevs
from collections import OrderedDict
import json 

def first_n_nonzero_digits(l, n):
    from itertools import islice
    return int(''.join(islice((i for i in str(abs(l)) if i not in {'0', '.'}), n)).ljust(n, '0'))

def formatNumberPDG(x):
    if x == 0: return "0"
    from math import floor,log10
    digits = first_n_nonzero_digits(x,3)
    scale = floor(log10(abs(x)))
    if digits < 354:
        fmt =  "{:."+str(int(max(0,-scale+1)))+"f}"
    elif digits < 949:
        fmt = "{:."+str(int(max(0,-scale)))+"f}"
    else:
        fmt = "{:."+str(int(max(0,-scale-1)))+"f}"
    return fmt.format(x)

def formatInterval(x):
    return "["+formatNumberPDG(x[0])+","+formatNumberPDG(x[1])+"]"

from RooFitUtils.pgfplotter import writematrix
from os.path import join as pjoin


sorting = ['cHq3',
           'cdHRe',
           'ceHRe',
           'c_EW_01',
           'c_EW_02',
           'c_EW_03',
           'cHe_cHl1_01',
           'cHu_cHd_cHq1_01',
           'cHu_cHd_cHq1_02',
           'cHl3_cll1_01',
           'cggH_1',
           'cggH_2',
           'ctop_01',
           ]

ev_names = ['cHq3',
           'cdHRe',
           'ceHRe',
           'c_EW_01',
           'c_EW_02',
           'c_EW_03',
#           'c_EW_04',
#           'c_EW_05',
#           'c_EW_06',
#           'c_EW_07',
           'cggH_1',
           'cggH_2',
#           'cggH_3',
           'cHe_cHl1_01',
#           'cHl1_cHe_02',
           'cHu_cHd_cHq1_01',
           'cHu_cHd_cHq1_02',
#           'cHu_cHd_cHq1_03',
           'cHl3_cll1_01',
#           'cHl3_cll1_02',
           'ctop_01',
#           'ctop_02',
#           'ctop_03',
#           'ctop_04',
#           'ctop_05',
#           'ctop_06',
#           'ctop_07',
#           'ctop_08',
#           'ctop_09',
#           'ctop_10',
#           'ctop_11',
#           'ctop_12',
#           'ctop_13'
]


eigenvalues = [
342381.404,
168765.476,
2395.284,
48.202,
43.475,
31.162,
15.944,
4.881,
3.359,
0.567,
0.364,
0.165,
0.055,
0.035,
0.027,
0.017,
0.01,
]    

parnames = [
    'cHq3',    
    'cdHRe',
    'ceHRe',
    'cHB',
    'cHW',
    'cHWB',
    'cuBRe',
    'cuWRe',
    'cHDD',
    'cHd',
    'cHu',
    'cHq1',
    'cHe',
    'cHl1',
    'cHl3',
    'cll1',
    'cHG',
    'cuGRe',
    'cG',
    'cqd8',
    'cqq1',
    'cqq11',
    'cqq3',
    'cqq31',
    'cqu1',
    'cqu8',
    'cuH',
    'cud8',
    'cuu',
    'cuu1'
]

ev_names.reverse()
evlabels = [ "{$\\lambda=" + str(eigenvalues[i]) + "$"+" \#" + str(i+1)+"}" for i in range(0,len(eigenvalues)) ]
evkeys = [ "ev"+str(i+1) for i in range(0,len(eigenvalues)) ]
eigenvalues.reverse()
evkeys.reverse()
evlabels.reverse()

def convert(ev_names,parnames,evs):
    allvals = [ ]
    for ev in ev_names:
        d = { e[1]:e[0] for e in evs[ev] }
        vals = []
        for p in parnames:
            if p in d.keys():
                vals.append(d[p])
            else:
                vals.append(0.)
        allvals.append(vals)
    return allvals


#parametrisation = convert(ev_names,parnames,evs)

from numpy import array
from numpy.linalg import inv,pinv

#allevs = list(fullevs.keys())
from RooFitUtils.io import collectresults#,collectcorrelations
from RooFitUtils.util import getThreshold,getPercent
t1sigma = getThreshold(0.68,1)
t2sigma = getThreshold(0.95,1)

labels=["$\\sqrt{s}=$13 TeV, 139 fb$^{\\scriptsize{-1}}$",
        "$m_{\\scalebox{.9}{$\\scriptstyle H$}}=$ 125.09 GeV, $|y_{\\scalebox{.9}{$\\scriptstyle H$}}|$ $<$ 2.5",
        "SMEFT $\\Lambda=1$ TeV"    ]

def getResults(values):
    from RooFitUtils.util import graphrescale,make1dscan
    from RooFitUtils.interpolate import findcrossings,findminimum,findintervals
    allpoints = make1dscan(values)
    nllmin = findminimum(allpoints)
    points = graphrescale(allpoints,nllmin,2)
    cv,dn,up = findcrossings(points,t1sigma)
#    print(t1sigma, t2sigma)
    i1s = findintervals(points,t1sigma)
    i2s = findintervals(points,t2sigma)
    return cv,i1s,i2s

colors = ["red","gray"]

def makeSummary(base,plottype,directories,ev_pois):
    scans = {}
    fitresults = {}
    paths = []
    results = {d:{"cv":{},"1s":{},"2s":{}} for d in directories }
    for name in ev_pois.keys():
        for d in directories:
            path = pjoin(base,d,plottype,"*"+name+"*_nll.txt*")
            collectresults(fitresults,path,"scan_"+d+"_"+name)
           # try:
            results[d]["cv"][name],results[d]["1s"][name],results[d]["2s"][name] = getResults(fitresults["scans"][(name,)]["scan_"+d+"_"+name])
            results[d]["cv"][name] =[results[d]["cv"][name]]
            print(results)

    pois100 = { k:v.copy() for k,v in ev_pois.items() if v.get("scale",1) > 10 }
    pois10 = { k:v.copy() for k,v in ev_pois.items() if v.get("scale",1) > 1 and v.get("scale",1) < 100}
    pois1 =  { k:v.copy() for k,v in ev_pois.items() if v.get("scale",1) == 1 }
    pois01 = { k:v.copy() for k,v in ev_pois.items() if v.get("scale",1) < 1 }
    
    for p in pois100.values():
        p["scale"] = p["scale"]/100
    for p in pois10.values():
        p["scale"] = p["scale"]/10
    for p in pois01.values():
        p["scale"] = p["scale"]*10

    data = []
    for i in range(0,len(directories)):
        d = directories[i]
        print(d)
        c = colors[i]
        offset = 7.5 * (1 - 2.*i/(len(directories)))
        data.append(({"point":True, "error":False,"interval":False,"style":"yshift="+str(offset)+"pt","color":c},results[d]["cv"]))
        data.append(({"point":False,"error":False,"interval":True,"style":"ultra thick,yshift="+str(offset)+"pt","color":c},results[d]["1s"]))
        data.append(({"point":False,"error":False,"interval":True,"style":"dashed,yshift="+str(offset)+"pt","color":c},results[d]["2s"]))
    from RooFitUtils.pgfplotter import writepois
    #print(pois10)
    #print(pois1)
    #iprint(pois01)
    #print(data)

    writepois("Internal",pois100,data,directories[0]+"_eftSummary_"+plottype+"_EV_100.tex",labels,range=[-.025,.025])
    writepois("",pois10,data,directories[0]+"_eftSummary_"+plottype+"_EV_10.tex",labels,range=[-.25,.25])
    writepois("",pois1,data,directories[0]+"_eftSummary_"+plottype+"_EV_1.tex" ,[""],range=[-2,2])
    writepois("",pois01,data,directories[0]+"_eftSummary_"+plottype+"_EV_01.tex",[""],range=[-10,10])
    
    return results

def makeSummaryFitResult(fitres,base,plottype,directories,ev_pois):
    scans = {}
    fitresults = {}
    paths = []
    fitdata = json.load(open(fitres))
    fitdata_results = fitdata["MLE"]["parameters"]
    fitdata_results_dict = { x["name"]:{"err":x["err"],"val":x["val"]} for x in fitdata_results}
    results = {d:{"cv":{},"1s":{},"2s":{}} for d in directories }
    for name in ev_pois.keys():
        for d in directories:
            path = pjoin(base,d,plottype,"*"+name+"*_nll.txt*")
           # collectresults(fitresults,path,"scan_"+d+"_"+name)
           # try:
            val, err = fitdata_results_dict[name]["val"], fitdata_results_dict[name]["err"]
            results[d]["cv"][name],results[d]["1s"][name],results[d]["2s"][name] = val, [(val-err,val+err)], [(val-2*err, val+2*err)]
            results[d]["cv"][name] =[results[d]["cv"][name]]

    pois100 = OrderedDict({ k:v.copy() for k,v in ev_pois.items() if v.get("scale",1) == 100})
    pois10  = OrderedDict({ k:v.copy() for k,v in ev_pois.items() if v.get("scale",1) == 10 })
    pois1   = OrderedDict( { k:v.copy() for k,v in ev_pois.items() if v.get("scale",1) == 1 })
    pois01  = OrderedDict({ k:v.copy() for k,v in ev_pois.items() if v.get("scale",1) < 1 })

    for p in pois100.values():
        p["scale"] = p["scale"]/100
    for p in pois10.values():
        p["scale"] = p["scale"]/10
    for p in pois01.values():
        p["scale"] = p["scale"]*10

    data = []
    for i in range(0,len(directories)):
        d = directories[i]
        print(d)
        c = colors[i]
        offset = 7.5 * (1 - 2.*i/(len(directories)))
        data.append(({"point":True, "error":False,"interval":False,"style":"yshift="+str(0)+"pt","color":c},results[d]["cv"]))
        data.append(({"point":False,"error":False,"interval":True,"style":"ultra thick,yshift="+str(0)+"pt","color":c},results[d]["1s"]))
        data.append(({"point":False,"error":False,"interval":True,"style":"dashed,yshift="+str(0)+"pt","color":c},results[d]["2s"]))
    from RooFitUtils.pgfplotter import writepois

    writepois("Internal",pois100,data,directories[0]+"_eftSummary_"+plottype+"_EV_100.tex",labels,range=[-.05,.05],spread=0.5)
    writepois("",pois10,data,directories[0]+"_eftSummary_"+plottype+"_EV_10.tex",labels,range=[-.3,.3],spread=0.5)
    writepois("",pois1,data,directories[0]+"_eftSummary_"+plottype+"_EV_1.tex" ,[""],range=[-3,3],spread=0.5)
    writepois("",pois01,data,directories[0]+"_eftSummary_"+plottype+"_EV_01.tex",[""],range=[-15,15],spread=0.5)
    
    return results
def ltx(s):
    return s.replace("_"," ")


def makeErrorTable(filename,tot,stat):
    from math import sqrt
    with open(filename,"wt") as table:
        table.write("\\documentclass{article}\n")
        table.write("\\usepackage{adjustbox}\n")
        table.write("\\usepackage{array}\n")
        table.write("\\usepackage{siunitx}\n")
        table.write("\\begin{document}\n")
        table.write("\\renewcommand\\arraystretch{1.5}\n")
        table.write("\\begin{adjustbox}{width=\\textwidth}\n")
        table.write("\\begin{tabular}{l S[table-format=3.2] l l l }\n")
        table.write("coefficient & obs cv & stat & sys & tot\\\\\n")
        for name in sorting:
            try:
                cvtot  = tot["cv"][name][0]
                cvstat = stat["cv"][name][0]
                table.write(name.replace("_"," "))
                table.write(
                    " &" + formatNumberPDG(cvtot)
                    + "&${}^{+"
                    + formatNumberPDG( stat["1s"][name][-1][1]-cvstat ) +"}_{"
                    + formatNumberPDG( stat["1s"][name][ 0][0]-cvstat ) +"}"
                    + "$&${}^{+"
                    + formatNumberPDG( sqrt(abs(pow(tot["1s"][name][-1][1]-cvtot,2) - pow(stat["1s"][name][-1][1]-cvstat,2))))+"}_{"
                    + formatNumberPDG(-sqrt(abs(pow(tot["1s"][name][ 0][0]-cvtot,2) - pow(stat["1s"][name][ 0][0]-cvstat,2))))+"}"
                    + "$&${}^{+"
                    + formatNumberPDG( tot["1s"][name][-1][1]-cvtot ) +"}_{"
                    + formatNumberPDG( tot["1s"][name][ 0][0]-cvtot ) +"}"
                    + "$")
                table.write("\\\\\n")
            except KeyError:
                continue
        table.write("\\end{tabular}\n")
        table.write("\\end{adjustbox}\n")
        table.write("\\end{document}\n")
        print("wrote "+filename)

def ltx(s):
    return s.replace("_"," ")

def makeCSV(filename,obs,exp,keys):
    with open(filename,"wt") as table:
        table.write("coefficient ")
        for k in keys:
            table.write(", obs "+ltx(k)+" cv , obs "+ltx(k)+" 68 , obs "+ltx(k)+" 95 , exp "+ltx(k)+" 68 , exp "+ltx(k)+" 95 ")
        table.write("\n")
        for name in sorting:
            table.write(name.replace("_"," "))
            for k in keys:
                table.write(
                    " ," + formatNumberPDG(obs[k]["cv"][name][0])
                    + "," + "v".join(["["+str(x[0])+","+str(x[1])+"]" for x in obs[k]["1s"][name]])
                    + "," + "v".join(["["+str(x[0])+","+str(x[1])+"]" for x in obs[k]["2s"][name]])
                    + "," + "v".join(["["+str(x[0])+","+str(x[1])+"]" for x in exp[k]["1s"][name]])
                    + "," + "v".join(["["+str(x[0])+","+str(x[1])+"]" for x in exp[k]["2s"][name]])
                )
            table.write("\n")

ev_names = ["CBHRE","CEHRE_CUHRE_CDHRE_CHBOX_01","C_HVV_VFF_01","C_HVV_VFF_02","C_HVV_VFF_03","C_HVV_VFF_04","C_HVV_VFF_05","C_HVV_VFF_06","C_HVV_VFF_07","C_HVV_VFF_08","C_HVV_VFF_09","C_HVV_VFF_10","C_HVV_VFF_11","C_HVV_VFF_12","C_HVV_VFF_13","C_HVV_VFF_14","C_HVV_VFF_15","C_HVV_VFF_16","CHG","C_2Q2L_01","C_2Q2L_02","C_4Q_01","CQJ31","CTGRE","CTHRE","C_TTH_TH_01","CW"]

ev_pois = OrderedDict({ ev:{} for ev in sorted(ev_names) })


ev_pois["CBHRE"]["scale"] = 10
ev_pois["CEHRE_CUHRE_CDHRE_CHBOX_01"]["scale"] = 0.1
ev_pois["C_HVV_VFF_01"]["scale"] =100 
ev_pois["C_HVV_VFF_02"]["scale"] =100  
ev_pois["C_HVV_VFF_03"]["scale"] =100  
ev_pois["C_HVV_VFF_04"]["scale"] =100  
ev_pois["C_HVV_VFF_05"]["scale"] =100  
ev_pois["C_HVV_VFF_06"]["scale"] = 10 
ev_pois["C_HVV_VFF_07"]["scale"] = 10
ev_pois["C_HVV_VFF_08"]["scale"] = 10
ev_pois["C_HVV_VFF_09"]["scale"] = 1
ev_pois["C_HVV_VFF_10"]["scale"] = 1
ev_pois["C_HVV_VFF_11"]["scale"] = 1
ev_pois["C_HVV_VFF_12"]["scale"] = 1
ev_pois["C_HVV_VFF_13"]["scale"] = 1
ev_pois["C_HVV_VFF_14"]["scale"] = 1
ev_pois["C_HVV_VFF_15"]["scale"] = 0.1
ev_pois["C_HVV_VFF_16"]["scale"] = 0.1
ev_pois["CHG"]["scale"] = 100
ev_pois["C_2Q2L_01"]["scale"] = 10
ev_pois["C_2Q2L_02"]["scale"] = 0.1
ev_pois["C_4Q_01"]["scale"] = 10
ev_pois["CQJ31"]["scale"] = 1
ev_pois["CTGRE"]["scale"] = 1
ev_pois["CTHRE"]["scale"] = 0.1
ev_pois["C_TTH_TH_01"]["scale"] = 1
ev_pois["CW"]["scale"] = 10

ev_pois["CBHRE"]["name"] = "CBHRE"
ev_pois["CEHRE_CUHRE_CDHRE_CHBOX_01"]["name"] = "CEHRE_CUHRE_CDHRE_CHBOX_01"
ev_pois["C_HVV_VFF_01"]["name"] = "C_HVV_VFF_01"
ev_pois["C_HVV_VFF_02"]["name"] = "C_HVV_VFF_02"
ev_pois["C_HVV_VFF_03"]["name"] = "C_HVV_VFF_03"
ev_pois["C_HVV_VFF_04"]["name"] = "C_HVV_VFF_04"
ev_pois["C_HVV_VFF_05"]["name"] = "C_HVV_VFF_05"
ev_pois["C_HVV_VFF_06"]["name"] = "C_HVV_VFF_06"
ev_pois["C_HVV_VFF_07"]["name"] = "C_HVV_VFF_07"
ev_pois["C_HVV_VFF_08"]["name"] = "C_HVV_VFF_08"
ev_pois["C_HVV_VFF_09"]["name"] = "C_HVV_VFF_09"
ev_pois["C_HVV_VFF_10"]["name"] = "C_HVV_VFF_10"
ev_pois["C_HVV_VFF_11"]["name"] = "C_HVV_VFF_11"
ev_pois["C_HVV_VFF_12"]["name"] = "C_HVV_VFF_12"
ev_pois["C_HVV_VFF_13"]["name"] = "C_HVV_VFF_13"
ev_pois["C_HVV_VFF_14"]["name"] = "C_HVV_VFF_14"
ev_pois["C_HVV_VFF_15"]["name"] = "C_HVV_VFF_15"
ev_pois["C_HVV_VFF_16"]["name"] = "C_HVV_VFF_16"
ev_pois["CHG"]["name"] = "CHG"
ev_pois["C_2Q2L_01"]["name"] = "C_2Q2L_01"
ev_pois["C_2Q2L_02"]["name"] = "C_2Q2L_02"
ev_pois["C_4Q_01"]["name"] = "C_4Q_01"
ev_pois["CQJ31"]["name"] = "CQJ31"
ev_pois["CTGRE"]["name"] = "CTGRE"
ev_pois["CTHRE"]["name"] = "CTHRE"
ev_pois["C_TTH_TH_01"]["name"] = "C_TTH_TH_01"
ev_pois["CW"]["name"] = "CW"

base = "scanResults/linear"
directories = ["exp","obs"]
#exp = makeSummary(base,"v0",directories,ev_pois)
#obs = makeSummary(base,"v0",directories,ev_pois)

#exp = makeSummary(base,"v0",["exp"],ev_pois)
#obs = makeSummary(base,"v0",["obs"],ev_pois)

#fitres = "/project/atlas/users/avisibil/project/HComb/EveryFingT/mvg_global_results_exp.json"
fitres = "out.json"
fitres = "out_one_at_a_time.json"
#fitres = "json_data.json"
exp = makeSummaryFitResult(fitres, base, "v0", ["exp_one_at_time"], ev_pois)

#exp["obs"] = obs["obs"]
#obs["exp"] = exp["exp"]


