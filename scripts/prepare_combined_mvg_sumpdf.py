#!/bin/env python
from senstiEFT.matrix import load_covariance, load_central_values, matrix, load_parameterisation, load_matrix
from senstiEFT.helpers import get_rotated_matrix
from senstiEFT.senstivity_helpers import make_infomat
from senstiEFT.matrix_helpers import add_list_of_matrices, get_padded_matrix
import numpy as np
import math 
import ROOT

_nodel = []
def nodel(something):
    """mark an object as undeletable for the garbage collector"""
    _nodel.append(something)

def make_mvg(parmat, rotmat, covmat, cenval, outfile, dataname="combData", wsname = "combWS", make_roosim = False):
    # prepare covariance in ROOT
    w = ROOT.RooWorkspace(wsname)
    poisset = ROOT.RooArgList()
    for ypar in rotmat.ypars:
        #print(ypar)
        w.factory("{0}[0,-1e3,1e3]".format(ypar))
        w.var(ypar).setError(0.001)
        poisset.add(w.var(ypar))

    for xpar in rotmat.xpars:
        w.factory(rotation_expr(rotmat,xpar))

    #print(parmat.ypars)
    #print(covmat.ypars)
    for ypar in parmat.ypars:
        w.factory(prepare_expr(parmat,ypar))

    n = len(covmat.matrix)
    mat1 = ROOT.TMatrixDSym(n)
    mat2 = ROOT.TMatrixDSym(n)
    for poi1,i in zip(sorted(covmat.xpars),range(n)):
        for poi2,j in zip(sorted(covmat.xpars), range(n)):
           if i==j: mat1[i][j] = 100*covmat.getElem(poi1,poi2)
           else: mat1[i][j] = 0.0

    for poi1,i in zip(sorted(covmat.xpars),range(n)):
        for poi2,j in zip(sorted(covmat.xpars), range(n)):
           mat2[i][j] = covmat.getElem(poi1,poi2)

    nodel(mat1)
    nodel(mat2)

    measpois_ral,valpois_ral1,eftpois_ral1 = ROOT.RooArgList(), ROOT.RooArgList(), ROOT.RooArgList()
    measpois_ras,valpois_ras,eftpois_ras = ROOT.RooArgSet() , ROOT.RooArgSet() , ROOT.RooArgSet()
    measpois_ral2,valpois_ral2,eftpois_ral2 = ROOT.RooArgList(), ROOT.RooArgList(), ROOT.RooArgList()
    #print(cenval.matrix)
    # prepare POIs
    for poipar,cenvalpar1 in zip(sorted(covmat.ypars), sorted(cenval.ypars)) :
        poivar = w.obj(poipar+"_new")
        if cenvalpar1 == poipar: cenvalparname1 = "cenval_1_{0}".format(poipar)
        else: cenvalparname1 = cenvalpar1
        cenvar1 = ROOT.RooRealVar(cenvalparname1, cenvalparname1,1.0) 
        cenvar1.setConstant(True) 
        nodel(poivar)
        nodel(cenvar1)
        measpois_ral.add(poivar)
        measpois_ras.add(poivar)
        valpois_ral1.add(cenvar1)
        valpois_ras.add(cenvar1)
    for poipar,cenvalpar2 in zip(sorted(covmat.ypars), sorted(cenval.ypars)) :
        if cenvalpar2 == poipar: cenvalparname2 = "cenval_2_{0}".format(poipar)
        else: cenvalparname2 = cenvalpar2
        cenvar2 = ROOT.RooRealVar(cenvalparname2, cenvalparname2, cenval.getElem("measured",cenvalpar2))
        cenvar2.setConstant(True) 
        nodel(poivar)
        nodel(cenvar2)
        valpois_ral2.add(cenvar2)
        valpois_ras.add(cenvar2)

    mvgpdf1 = ROOT.RooMultiVarGaussian("mvg1","mvg1",measpois_ral,valpois_ral1,mat1)
    mvgpdf2 = ROOT.RooMultiVarGaussian("mvg2","mvg2",measpois_ral,valpois_ral2,mat2)
    pdflist = ROOT.RooArgList()
    nodel(mvgpdf1)
    nodel(mvgpdf2)
    pdflist.add(mvgpdf1)
    pdflist.add(mvgpdf2)
    const1 = ROOT.RooRealVar("const1", "const1", 0.5)
    const2 = ROOT.RooRealVar("const2", "const2", 0.5)
    coeffs = ROOT.RooArgList()
    coeffs.add(const1)
    coeffs.add(const2)
    pdf = ROOT.RooRealSumPdf("prod","prod", pdflist, coeffs)

    pdf.Print()
    mc = ROOT.RooStats.ModelConfig("ModelConfig",w)

    if args.roosim:
        testcat = ROOT.RooCategory("category","category")
        testcat.defineType("test")
        dat = ROOT.RooDataSet(dataname+"_tmp", dataname+"_tmp",valpois_ras)
        dat.add(valpois_ras)
        simdata = ROOT.RooDataSet(dataname,dataname,valpois_ras,ROOT.RooFit.Index(testcat),ROOT.RooFit.Import("test",dat))
        simPdf = ROOT.RooSimultaneous("simPdf","simultaneous pdf",testcat)
        simPdf.addPdf(pdf,"test")
        mc.SetPdf(simPdf)           
        getattr(w,'import')(simdata) 

    else:
        dat = ROOT.RooDataSet(dataname, dataname,valpois_ras)
        dat.add(valpois_ras)
        mc.SetPdf(pdf)
        getattr(w,'import')(dat)

    mc.SetParametersOfInterest(poisset)#measpois_ras)
    #mc.SetSnapshot(measpois_ras)
    mc.SetObservables(valpois_ras)
    mc.SetGlobalObservables(valpois_ras)
    getattr(w,'import')(mc)

    print("writing workspace to "+outfile)
    w.writeToFile(outfile,True)
    

def print_stddev(covmat):
    for x, i in zip(covmat.xpars,range(len(covmat.xpars))):
        print("{0} : {1:.4f}".format(x,math.sqrt(covmat.matrix[i][i])))

def make_combined_mat(list_of_mats):
    xpars, ypars = [], []
    # prepare the axes of the matrix
    for mat in list_of_mats:
        for xpar in mat.xpars:
            if xpar not in xpars: xpars.append(xpar)
        for ypar in mat.ypars:
            if ypar not in ypars: ypars.append(ypar)
    # create empty matrix 
    matarr = np.zeros((len(ypars), len(xpars)))
    name = "combined_{0}".format("_".join([mat.name for mat in list_of_mats]))
    combined_mat = matrix(name, matarr, xpars=xpars, ypars=ypars)
    
    # collect the matrix 
    for mat in list_of_mats:
        for xpar in xpars:
            for ypar in ypars:
                if xpar in mat.xpars and ypar in mat.ypars:
                     combined_mat.setElem(xpar, ypar, mat.getElem(xpar, ypar))

    return combined_mat 
     

def make_combined_mvg(args):
    covmats = [ load_covariance(x[2]) for x in args.mvg_inputs ]
    cenvals = [ load_central_values(x[2],args.expected) for x in args.mvg_inputs ]
    parmats = [ load_parameterisation(x[1]) for x in args.mvg_inputs ]
    for parmat in parmats:
        parmat.xpars = [x.upper() for x in parmat.xpars]

    covmat, cenval, parmat = make_combined_mat(covmats), make_combined_mat(cenvals), make_combined_mat(parmats)
    parmat.save_matrix("combined_param.json")
    covmat.save_matrix("combined_covmat.json")
    if args.rotation != "":
        rotmat = load_matrix(args.rotation)

    else:
        xpars = [x for x in parmat.xpars if "*" not in x if x != "SM"]
        n = len(xpars) 
        identity = np.zeros((n,n), int)
        np.fill_diagonal(identity, 1)
        rotmat = matrix("rotation",identity, xpars=xpars, ypars=xpars)
    #print(cenval.matrix, cenval.ypars)
    make_mvg(parmat, rotmat, covmat, cenval, args.output, dataname=args.data, wsname = args.ws, make_roosim = args.roosim)

def rotation_expr(mat, xpar):
    expr = ""
    for ypar,i in zip(mat.ypars,range(len(mat.ypars))):
        val = mat.getElem(xpar,ypar)
        absval = abs(val)
        if val >= 0.0 : sign = " + "
        else: sign = " - "
        expr = expr + " {0} {1}*@{2}".format(sign,absval,i)

    parlist = ",".join(mat.ypars)
    return "expr::{0}('{1}',{2})".format(xpar, expr, parlist)


def prepare_expr(mat, ypar_fin):
    pars = [par for par in mat.xpars if "*" not in par and par != "SM"]
    for ypar in mat.ypars:
        str0 = ""
        pars_in_form = []
        i = 0
        for par1 in pars:
            val = mat.getElem(par1, ypar_fin)#/mat.getElem("SM",ypar)
            if abs(val) >= 1e-5:
                if val > 0.0: sign = " + "
                else: sign = " - "
                str0 = " {0} {1:.5f}*@{2} ".format(sign,abs(val),i) + str0  
                if par1 not in pars_in_form:
                    pars_in_form.append(par1)
                    i = i+1
    
        for par1 in pars:
            for par2 in pars[pars.index(par1):]:
                term = "{0}*{1}".format(par1,par2)
                if term not in mat.xpars: continue
                val = mat.getElem(term, ypar_fin)#/mat.getElem("SM",ypar)
                if abs(val) >= 1e-5:
                    if par2 not in pars_in_form:
                        pars_in_form.append(par2)
                        i = i+1
                    if par1 not in pars_in_form:
                        pars_in_form.append(par1)
                        i = i+1
                    if val > 0.0: sign = " + "
                    else: sign = " - "
                    str0 = " {0} {1:.5f}*@{2}*@{3}".format(sign,abs(val),pars_in_form.index(par1),pars_in_form.index(par2)) + str0

        if "SM" in mat.xpars: smval = mat.getElem("SM",ypar_fin)
        else: smval = 1.0 
       
        return "expr::{0}_new('{3} {1}',{2})".format(ypar_fin,str0,",".join(pars_in_form),smval)

                             
if __name__ == '__main__':   
    from argparse import ArgumentParser
    parser = ArgumentParser("create multi-variate gaussian RooWorkspace")
    parser.add_argument('--input',action='append',dest="mvg_inputs",nargs="+",help="files with input information",required=True)
    parser.add_argument('-o','--output',help="output ROOT file",required=True)
    parser.add_argument( "--pois",nargs="+"     , type=str,     dest="pois"                 , help="list of POI names", default=[])
    parser.add_argument( "--workspace",           type=str,  dest="ws", help="name of output workspace"     , required=False,default="combWS")    
    parser.add_argument( "--param",              type=str,  dest="param", help="name of output workspace"     , required=False,default="")    
    parser.add_argument( "--rotation",              type=str,  dest="rotation", help="name of output workspace"     , required=False,default="")    
    parser.add_argument( "--expected",            action='store_true',  help="create an expected workspace"   , required=False)    
    parser.add_argument( "--data",                type=str,  help="name of dataset in RooFit workspace"   , required=False, default="combData")    
    parser.add_argument( "--make-simultaneous"    , dest="roosim", action="store_true",  help="patch the mvg as a roosimpdf" ,default=False)    

    args = parser.parse_args()
    make_combined_mvg(args)

