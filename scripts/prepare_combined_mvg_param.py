#!/bin/env python
from senstiEFT.matrix import load_covariance, load_central_values, matrix, load_parameterisation, load_matrix
from senstiEFT.helpers import get_rotated_matrix
from senstiEFT.senstivity_helpers import make_infomat
from senstiEFT.matrix_helpers import add_list_of_matrices, get_padded_matrix
import numpy as np
import math 
import ROOT

_nodel = []
def nodel(something):
    """mark an object as undeletable for the garbage collector"""
    _nodel.append(something)

def make_mvg(parmat, covmat1, cenval1, outfile, dataname="combData", wsname = "combWS", make_roosim = False):
    # prepare covariance in ROOT
    #print(cenval.xpars)
    w = ROOT.RooWorkspace(wsname)
    poisset = ROOT.RooArgList()
    for xpar in parmat.xpars:
        w.factory("{0}[0,-1e6,1e6]".format(xpar))
        w.var(xpar).setError(0.001)
        poisset.add(w.var(xpar))
    for ypar in parmat.ypars:
        w.factory(prepare_expr(parmat,ypar))
    n1 = len(covmat1.matrix)
    mat1 = ROOT.TMatrixDSym(n1)
    for poi1,i in zip(sorted(covmat1.xpars),range(n1)):
        for poi2,j in zip(sorted(covmat1.xpars), range(n1)):
           mat1[i][j] = covmat1.getElem(poi1,poi2)

    nodel(mat1)

    measpois_ral,valpois_ral1,eftpois_ral1 = ROOT.RooArgList(), ROOT.RooArgList(), ROOT.RooArgList()
    measpois_ras,valpois_ras,eftpois_ras = ROOT.RooArgSet() , ROOT.RooArgSet() , ROOT.RooArgSet()
    # prepare POIs
    #print(parmat.ypars, cenval.ypars) 
    for poipar,cenvalpar1 in zip(sorted(covmat1.ypars), sorted(cenval1.ypars)) :
        #poivarpoipar, poipar, cenval1.getElem("measured",cenvalpar1), -1e6, 1e6) 
        #poivar.setError(0.0001)
        poivar = w.obj(poipar+"_new")
        print(poivar)
        if cenvalpar1 == poipar: cenvalparname1 = "cenval_1_{0}".format(poipar)
        else: cenvalparname1 = cenvalpar1
        cenvar1 = ROOT.RooRealVar(cenvalparname1, cenvalparname1, cenval1.getElem("measured",cenvalpar1)) 
        cenvar1.setConstant(True) 
        nodel(poivar)
        nodel(cenvar1)
    #    measpois_ral.add(poivar)
    #    measpois_ras.add(poivar)
        measpois_ral.add(poivar)
        measpois_ras.add(poivar)
        valpois_ral1.add(cenvar1)
        valpois_ras.add(cenvar1)

    mvgpdf1 = ROOT.RooMultiVarGaussian("mvg1","mvg1",measpois_ral,valpois_ral1,mat1)
    nodel(mvgpdf1)
    #mvgpdf.Print() 
    mc = ROOT.RooStats.ModelConfig("ModelConfig",w)

    if args.roosim:
        testcat = ROOT.RooCategory("category","category")
        testcat.defineType("test")
        dat = ROOT.RooDataSet(dataname+"_tmp", dataname+"_tmp",valpois_ras)
        dat.add(valpois_ras)
        simdata = ROOT.RooDataSet(dataname,dataname,valpois_ras,ROOT.RooFit.Index(testcat),ROOT.RooFit.Import("test",dat))
        simPdf = ROOT.RooSimultaneous("simPdf","simultaneous pdf",testcat)
        simPdf.addPdf(mvgpdf1,"test")
        mc.SetPdf(simPdf)           
        getattr(w,'import')(simdata) 

    else:
        dat = ROOT.RooDataSet(dataname, dataname,valpois_ras)
        dat.add(valpois_ras)
        mc.SetPdf(mvgpdf1)
        getattr(w,'import')(dat)

    nps = ROOT.RooArgSet()
    dummy = ROOT.RooRealVar("dummy","dummy",-1,1)
    nps.add(dummy)

    mc.SetNuisanceParameters(nps)
    mc.SetParametersOfInterest(poisset)#measpois_ras)
    mc.SetSnapshot(measpois_ras)
    mc.SetObservables(valpois_ras)
    mc.SetGlobalObservables(valpois_ras)
    getattr(w,'import')(mc)

    print("writing workspace to "+outfile)
    w.writeToFile(outfile,True)
    

def print_stddev(covmat):
    for x, i in zip(covmat.xpars,range(len(covmat.xpars))):
        print("{0} : {1:.4f}".format(x,math.sqrt(covmat.matrix[i][i])))

def make_combined_mat(list_of_mats):
    xpars, ypars = [], []
    # prepare the axes of the matrix
    for mat in list_of_mats:
        for xpar in mat.xpars:
            if xpar not in xpars: xpars.append(xpar)
        for ypar in mat.ypars:
            if ypar not in ypars: ypars.append(ypar)
    # create empty matrix 
    matarr = np.zeros((len(ypars), len(xpars)))
    name = "combined_{0}".format("_".join([mat.name for mat in list_of_mats]))
    combined_mat = matrix(name, matarr, xpars=xpars, ypars=ypars)
    
    # collect the matrix 
    for mat in list_of_mats:
        for xpar in xpars:
            for ypar in ypars:
                if xpar in mat.xpars and ypar in mat.ypars:
                     #print(xpar, ypar, mat.getElem(xpar, ypar))
                     #print(len(mat.matrix), len(mat.matrix[0]), len(mat.xpars), len(mat.ypars)) 
                     combined_mat.setElem(xpar, ypar, mat.getElem(xpar, ypar))

    return combined_mat 
     

def make_combined_mvg(args):
    covmats = [ load_covariance(x[1]) for x in args.input ]
    cenvals = [ load_central_values(x[1],args.expected) for x in args.input ]
    covmat1, cenval1 = make_combined_mat(covmats), make_combined_mat(cenvals)

    parmat = load_matrix(args.param)
    if args.rotation != "":
        rotmat = load_matrix(args.rotation)
        parmat_rot = get_rotated_matrix(parmat, rotmat, "rotated_param")
        parmat = parmat_rot

    make_mvg(parmat, covmat1, cenval1, args.output, dataname=args.data, wsname = args.ws, make_roosim = args.roosim)

def prepare_expr(parmat, ypar):
    expr = "1.0  "
    for xpar,i in zip(parmat.xpars,range(len(parmat.xpars))):
        val = parmat.getElem(xpar,ypar)
        absval = abs(val)
        if val >= 0.0 : sign = " + "
        else: sign = " - "
        expr = expr + " {0} {1}*@{2}".format(sign,absval,i)

    parlist = ",".join(parmat.xpars)

    return "expr::{0}_new('{1}',{2})".format(ypar, expr, parlist)


                             
if __name__ == '__main__':   
    from argparse import ArgumentParser
    parser = ArgumentParser("create multi-variate gaussian RooWorkspace")
    parser.add_argument('-i1','--input',action='append',nargs="+",help="files with input information",required=True)
    parser.add_argument('-o','--output',help="output ROOT file",required=True)
    parser.add_argument( "--pois",nargs="+"     , type=str,     dest="pois"                 , help="list of POI names", default=[])
    parser.add_argument( "--workspace",           type=str,  dest="ws", help="name of output workspace"     , required=False,default="combWS")    
    parser.add_argument( "--param",              type=str,  dest="param", help="name of output workspace"     , required=False,default="")    
    parser.add_argument( "--rotation",              type=str,  dest="rotation", help="name of output workspace"     , required=False,default="")    
    parser.add_argument( "--expected",            action='store_true',  help="create an expected workspace"   , required=False)    
    parser.add_argument( "--data",                type=str,  help="name of dataset in RooFit workspace"   , required=False, default="combData")    
    parser.add_argument( "--make-simultaneous"    , dest="roosim", action="store_true",  help="patch the mvg as a roosimpdf" ,default=False)    

    args = parser.parse_args()
    make_combined_mvg(args)

