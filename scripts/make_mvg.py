#!/bin/env python
_nodel = []
def nodel(something):
    """mark an object as undeletable for the garbage collector"""
    _nodel.append(something)

def makeResult(input,poinames,expected):
    import yaml
    import ROOT
    mvg_dict = yaml.safe_load(open(input)) 

    covarr = mvg_dict['covariance']
    if 'names' in mvg_dict.keys(): allpars = mvg_dict['names']
    ## bin0,bin1,... if names not given
    else: allpars = [ "bin{0}".format(i) for i in range(len(mvg_dict['measured'])) ]
    if len(poinames) == 0: poinames = allpars
    measvals = mvg_dict['measured']
    if expected: vals = mvg_dict['sm']
    else: vals = measvals

    poilist, poiset = ROOT.RooArgList(), ROOT.RooArgSet()
    cvlist, cvset = ROOT.RooArgList(), ROOT.RooArgSet()
  
    for poi,cenval in zip(poinames,vals):
       
        val_name = "val_"+poi
        poivar = ROOT.RooRealVar(poi,poi,-1e6,1e6)
        poivar.setError(0.1)
        measvar =  ROOT.RooRealVar(val_name,val_name,cenval)
        nodel(poivar)
        nodel(measvar)
        poilist.add(poivar)
        poiset.add(poivar) 
        cvlist.add(measvar)
        cvset.add(measvar)

    n = len(poinames)
    mat = ROOT.TMatrixDSym(n)
    for i,poi1 in zip(range(n),poinames):
        for j,poi2 in zip(range(n),poinames):
           mat[i][j] = covarr[allpars.index(poi1)][allpars.index(poi2)]

    nodel(mat)

    return {
        "poilist": poilist,
        "poiset": poiset,
        "cvlist": cvlist,
        "cvset": cvset,
        "covmat": mat
    }

def make_mvg(args):
    import ROOT
    result = makeResult(args.inResult,args.pois,args.expected)
    mvgpdf = ROOT.RooMultiVarGaussian("mvg","mvg",result["poilist"],result["cvlist"],result["covmat"])
    mvgpdf.Print() 
    dat = ROOT.RooDataSet(args.data, '',result["cvset"])
    dat.add(result["cvset"])
    w = ROOT.RooWorkspace(args.workspace)
    mc = ROOT.RooStats.ModelConfig("ModelConfig",w)
    mc.SetPdf(mvgpdf)
    mc.SetParametersOfInterest(result["poiset"])
    mc.SetSnapshot(result["poiset"])
    mc.SetObservables(result["cvset"])    
    # mc.SetGlobalObservables(result["cvset"])
    getattr(w,'import')(mc)
    getattr(w,'import')(dat)

    print("writing workspace to "+args.outWS)
    w.writeToFile(args.outWS,True)
    
if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser("create multi-variate gaussian RooWorkspace")
    parser.add_argument( "-i","--input"         , type=str,     dest="inResult"                 , help="input file with result", required=True, metavar="path/to/file")
    parser.add_argument( "--pois",nargs="+"     , type=str,     dest="pois"                 , help="list of POI names", default=[])
    parser.add_argument( "-o","--output"        , type=str,     dest="outWS"                  , help="path to output workspace"     , required=False,default="out.root")
    parser.add_argument( "--workspace",           type=str,  help="name of output workspace"     , required=False,default="combWS")    
    parser.add_argument( "--expected",            action='store_true',  help="create an expected workspace"   , required=False)    
    parser.add_argument( "--data",                type=str,  help="name of dataset in RooFit workspace"   , required=True)    

    args = parser.parse_args()

    make_mvg(args)

