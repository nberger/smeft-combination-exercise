#!/bin/env python
from senstiEFT.matrix import load_covariance, load_central_values, matrix
from senstiEFT.senstivity_helpers import make_infomat
from senstiEFT.matrix_helpers import add_list_of_matrices, get_padded_matrix
import numpy as np
import math 
import ROOT

_nodel = []
def nodel(something):
    """mark an object as undeletable for the garbage collector"""
    _nodel.append(something)

def make_mvg(covmat, cenval, outfile, dataname="combData", wsname = "combWS"):
    # prepare covariance in ROOT
    #print(cenval.xpars)
    w = ROOT.RooWorkspace(wsname)
    n = len(covmat.matrix)
    mat = ROOT.TMatrixDSym(n)
    for poi1,i in zip(covmat.xpars,range(n)):
        for poi2,j in zip(covmat.xpars, range(n)):
           mat[i][j] = covmat.getElem(poi1,poi2)

    nodel(mat)

    measpois_ral,valpois_ral,eftpois_ral = ROOT.RooArgList(), ROOT.RooArgList(), ROOT.RooArgList()
    measpois_ras,valpois_ras,eftpois_ras = ROOT.RooArgSet() , ROOT.RooArgSet() , ROOT.RooArgSet()

    # prepare POIs
    #print(parmat.ypars, cenval.ypars) 
    for poipar,cenvalpar in zip(covmat.ypars, cenval.ypars) :
        poivar = ROOT.RooRealVar(poipar, poipar, cenval.getElem("measured",cenvalpar), -1e6, 1e6) 
        poivar.setError(0.0001)
        if cenvalpar == poipar: cenvalparname = "cenval_{0}".format(poipar)
        else: cenvalparname = cenvalpar
        cenvar = ROOT.RooRealVar(cenvalparname, cenvalparname, cenval.getElem("measured",cenvalpar)) 
        nodel(poivar)
        nodel(cenvar)
        measpois_ral.add(poivar)
        measpois_ras.add(poivar)
        valpois_ral.add(cenvar)
        valpois_ras.add(cenvar)

    #print("creating Pdf")
    #measpois_ral = 
    mvgpdf = ROOT.RooMultiVarGaussian("mvg","mvg",measpois_ral,valpois_ral,mat)
    #mvgpdf.Print() 
    dat = ROOT.RooDataSet(dataname, dataname,valpois_ras)
    dat.add(valpois_ras)

    mc = ROOT.RooStats.ModelConfig("ModelConfig",w)
    mc.SetPdf(mvgpdf)
    mc.SetParametersOfInterest(measpois_ras)
    mc.SetSnapshot(measpois_ras)
    mc.SetObservables(valpois_ras)
    mc.SetGlobalObservables(valpois_ras)
    getattr(w,'import')(mc)
    getattr(w,'import')(dat)

    print("writing workspace to "+outfile)
    w.writeToFile(outfile,True)
    

def print_stddev(covmat):
    for x, i in zip(covmat.xpars,range(len(covmat.xpars))):
        print("{0} : {1:.4f}".format(x,math.sqrt(covmat.matrix[i][i])))


def make_combined_mat(list_of_mats):
    xpars, ypars = [], []
    # prepare the axes of the matrix
    for mat in list_of_mats:
        for xpar in mat.xpars:
            if xpar not in xpars: xpars.append(xpar)
        for ypar in mat.ypars:
            if ypar not in ypars: ypars.append(ypar)
    # create empty matrix 
    matarr = np.zeros((len(ypars), len(xpars)))
    for mat in list_of_mats: print(mat.name)
    name = "combined_{0}".format("_".join([mat.name for mat in list_of_mats]))
    combined_mat = matrix(name, matarr, xpars=xpars, ypars=ypars)
    
    # collect the matrix 
    for mat in list_of_mats:
        for xpar in xpars:
            for ypar in ypars:
                if xpar in mat.xpars and ypar in mat.ypars:
                     #print(xpar, ypar, mat.getElem(xpar, ypar))
                     #print(len(mat.matrix), len(mat.matrix[0]), len(mat.xpars), len(mat.ypars)) 
                     combined_mat.setElem(xpar, ypar, mat.getElem(xpar, ypar))

    return combined_mat 
     

def make_combined_mvg(args):
    covmats = [ load_covariance(x[1]) for x in args.input ]
    cenvals = [ load_central_values(x[1],args.expected) for x in args.input ]
    covmat, cenval = make_combined_mat(covmats), make_combined_mat(cenvals)
    make_mvg(covmat, cenval, args.output, dataname=args.data, wsname = args.ws)
                             
if __name__ == '__main__':   
    from argparse import ArgumentParser
    parser = ArgumentParser("create multi-variate gaussian RooWorkspace")
    parser.add_argument('-i','--input',action='append',nargs="+",help="files with input information",required=True)
    parser.add_argument('-o','--output',help="output ROOT file",required=True)
    parser.add_argument( "--pois",nargs="+"     , type=str,     dest="pois"                 , help="list of POI names", default=[])
    parser.add_argument( "--workspace",           type=str,  dest="ws", help="name of output workspace"     , required=False,default="combWS")    
    parser.add_argument( "--expected",            action='store_true',  help="create an expected workspace"   , required=False)    
    parser.add_argument( "--data",                type=str,  help="name of dataset in RooFit workspace"   , required=False, default="combData")    

    args = parser.parse_args()
    make_combined_mvg(args)

