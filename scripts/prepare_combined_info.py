from senstiEFT.matrix import load_parameterisation, load_covariance
from senstiEFT.senstivity_helpers import make_infomat
from senstiEFT.matrix_helpers import add_list_of_matrices, get_padded_matrix
import math 

def print_stddev(covmat):
    for x, i in zip(covmat.xpars,range(len(covmat.xpars))):
        print("{0} : {1:.4f}".format(x,math.sqrt(covmat.matrix[i][i])))

def prepare_combined_info(args):
    parmats = [ load_parameterisation(x[1]) for x in args.input ]
    covmats = [ load_covariance(x[2]) for x in args.input ]
    infomats = [ make_infomat(x[0], covmat, parmat) for x, covmat, parmat in zip(args.input, covmats, parmats)]
    xpars = list(set([xpar for mat in infomats for xpar in mat.xpars]))
    padded_infomats = [ get_padded_matrix(infomat, pad_name=infomat.name, pad_xpars=xpars, pad_ypars=xpars) for infomat in infomats ] 
    if args.stddev_print: print_stddev(covmats[0])
    combined_infomat = add_list_of_matrices(padded_infomats)
    combined_infomat.name = "infomat_{0}".format("_".join([x[0] for x in args.input]))
    print(combined_infomat.xpars)
    if args.output.endswith(".json") : combined_infomat.save_matrix(args.output)
    else: print("Currently support only saving the matrix as json")

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description="prepare combined information matrix, used for the sensitivity study")
    parser.add_argument('-i','--input',action='append',nargs="+",help="files with input information",required=True)
    parser.add_argument('-o','--output',help="output information matrix",required=True)
    parser.add_argument('--print-stddev',action='store_true',dest='stddev_print',help="print the stddev of bins",default=False)
    args = parser.parse_args()
    print(args.input)
    info = prepare_combined_info(args)


