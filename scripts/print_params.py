#!/bin/env python
from senstiEFT.matrix import load_parameterisation
from senstiEFT.senstivity_helpers import make_infomat
from senstiEFT.matrix_helpers import add_list_of_matrices, get_padded_matrix
import numpy as np
import math 
import ROOT

def compare_pars(pars1, pars2):
    for x in pars1:
        if x.upper() not in [y.upper() for y in pars2]:
           print("{0} is not common!".format(x))

def common_pars(pars1, pars2):
    return [ x for x in pars1 if x in pars2]

def compare_params(args):
    parmat = load_parameterisation(args.param)
    parmat.xpars = [x.upper() for x in sorted(parmat.xpars)]
    ypars = sorted(parmat.ypars)
    xpars = parmat.xpars
    for ypar in ypars:
        str0  = ypar.ljust(42)+" : "        
        for xpar in xpars:
            val = parmat.getElem(xpar, ypar) 
            if val < 0 : sign = " - "
            else: sign = " + "
         #   if args.fractional and val != 0.0: 
         #        val =100*val/ parmat1.getElem(xpar, ypar)
            if val != 0.0: str0 = str0 + "{0} {1:.3f} {2}".format(sign, abs(val), xpar).ljust(15)
        print(str0)


if __name__ == '__main__':   
    from argparse import ArgumentParser
    parser = ArgumentParser(" ")
    parser.add_argument( "--param"    ,           type=str,  dest="param", help="name of output workspace"     , required=False,default="")    

    args = parser.parse_args()
    compare_params(args)
