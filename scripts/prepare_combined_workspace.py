#!/bin/env python
from senstiEFT.matrix import load_parameterisation, load_covariance, load_central_values, matrix, load_matrix
from senstiEFT.senstivity_helpers import make_infomat
from senstiEFT.helpers import get_rotated_matrix
from senstiEFT.matrix_helpers import add_list_of_matrices, get_padded_matrix
import numpy as np
import math 
import ROOT

_nodel = []
def nodel(something):
    """mark an object as undeletable for the garbage collector"""
    _nodel.append(something)

def makeResult(input,poinames,expected):
    mvg_dict = yaml.safe_load(open(input)) 

    covarr = mvg_dict['covariance']
    if 'names' in mvg_dict.keys(): allpars = mvg_dict['names']
    ## bin0,bin1,... if names not given
    else: allpars = [ "bin{0}".format(i) for i in range(len(mvg_dict['measured'])) ]
    if len(poinames) == 0: poinames = allpars
    measvals = mvg_dict['measured']
    if expected: vals = mvg_dict['sm']
    else: vals = measvals

    poilist, poiset = ROOT.RooArgList(), ROOT.RooArgSet()
    cvlist, cvset = ROOT.RooArgList(), ROOT.RooArgSet()
  
    for poi,cenval in zip(poinames,vals):
       
        val_name = "val_"+poi
        poivar = ROOT.RooRealVar(poi,poi,cenval,-1e6,1e6)
        poivar.setError(0.1)
        measvar =  ROOT.RooRealVar(val_name,val_name,cenval)
        nodel(poivar)
        nodel(measvar)
        poilist.add(poivar)
        poiset.add(poivar) 
        cvlist.add(measvar)
        cvset.add(measvar)

    n = len(poinames)
    mat = ROOT.TMatrixDSym(n)
    for i,poi1 in zip(range(n),poinames):
        for j,poi2 in zip(range(n),poinames):
           mat[i][j] = covarr[allpars.index(poi1)][allpars.index(poi2)]

    nodel(mat)

    return {
        "poilist": poilist,
        "poiset": poiset,
        "cvlist": cvlist,
        "cvset": cvset,
        "covmat": mat
    }

def make_mvg(covmat, cenval, parmat, outfile, dataname="combData", wsname = "combWS"):
    # prepare covariance in ROOT
    #print(cenval.xpars)
    ws = ROOT.RooWorkspace(wsname)
    poisset = ROOT.RooArgSet()

        
    for xpar in parmat.xpars:
        ws.factory("{0}[0,-1e6,1e6]".format(xpar))
        ws.var(xpar).setError(0.001)
        poisset.add(ws.var(xpar))
    for ypar in parmat.ypars:
        ws.factory(prepare_expr(parmat,ypar))
    n = len(covmat.matrix)
    mat = ROOT.TMatrixDSym(n)
    for poi1,i in zip(covmat.xpars,range(n)):
        for poi2,j in zip(covmat.xpars, range(n)):
           mat[i][j] = covmat.getElem(poi1,poi2)

    nodel(mat)

    measpois_ral,valpois_ral,eftpois_ral = ROOT.RooArgList(), ROOT.RooArgList(), ROOT.RooArgList()
    measpois_ras,valpois_ras,eftpois_ras = ROOT.RooArgSet() , ROOT.RooArgSet() , ROOT.RooArgSet()

    # prepare POIs
    #print(parmat.ypars, cenval.ypars) 
    for poipar,cenvalpar in zip(parmat.ypars, cenval.ypars) :
        if cenvalpar == poipar: cenvalparname = "cenval_{0}".format(poipar)
        else: cenvalparname = cenvalpar
        cenvar = ROOT.RooRealVar(cenvalparname, cenvalparname, cenval.getElem("measured",cenvalpar)) 
        nodel(cenvar)
        measpois_ral.add(ws.obj(poipar+"_new"))
        measpois_ras.add(ws.obj(poipar+"_new"))
        valpois_ral.add(cenvar)
        valpois_ras.add(cenvar)

    mvgpdf = ROOT.RooMultiVarGaussian("mvg","mvg",measpois_ral,valpois_ral,mat)
    dat = ROOT.RooDataSet(dataname, dataname,valpois_ras)
    dat.add(valpois_ras)

    mc = ROOT.RooStats.ModelConfig("ModelConfig",ws)
    mc.SetPdf(mvgpdf)
    mc.SetParametersOfInterest(poisset)
    mc.SetSnapshot(poisset)
    mc.SetObservables(valpois_ras)
    mc.SetGlobalObservables(valpois_ras)
    getattr(ws,'import')(mc)
    getattr(ws,'import')(dat)

    print("writing workspace to "+outfile)
    ws.writeToFile(outfile,True)
    

def print_stddev(covmat):
    for x, i in zip(covmat.xpars,range(len(covmat.xpars))):
        print("{0} : {1:.4f}".format(x,math.sqrt(covmat.matrix[i][i])))


def make_combined_mat(list_of_mats):
    xpars, ypars = [], []
    # prepare the axes of the matrix
    for mat in list_of_mats:
        for xpar in mat.xpars:
            if xpar not in xpars: xpars.append(xpar)
        for ypar in mat.ypars:
            if ypar not in ypars: ypars.append(ypar)
    # create empty matrix 
    matarr = np.zeros((len(ypars), len(xpars)))
    name = "combined_{0}".format("_".join([mat.name for mat in list_of_mats]))
    combined_mat = matrix(name, matarr, xpars=xpars, ypars=ypars)
    
    # collect the matrix 
    for mat in list_of_mats:
        for xpar in xpars:
            for ypar in ypars:
                if xpar in mat.xpars and ypar in mat.ypars:
                     #print(xpar, ypar, mat.getElem(xpar, ypar))
                     #print(len(mat.matrix), len(mat.matrix[0]), len(mat.xpars), len(mat.ypars)) 
                     combined_mat.setElem(xpar, ypar, mat.getElem(xpar, ypar))

    return combined_mat 
     

def make_combined_mvg(args):
    parmats = [ load_parameterisation(x[1]) for x in args.input ]
    covmats = [ load_covariance(x[2]) for x in args.input ]
    cenvals = [ load_central_values(x[2],args.expected) for x in args.input ]
    parmat, covmat, cenval = make_combined_mat(parmats), make_combined_mat(covmats), make_combined_mat(cenvals)
    if args.rotation != "":
        rotmat = load_matrix(args.rotation)
        parmat_rot = get_rotated_matrix(parmat, rotmat, "rotated_param")
        parmat = parmat_rot

    make_mvg(covmat, cenval, parmat, args.output, dataname=args.data, wsname = args.ws)

def prepare_expr(parmat, ypar):
    expr = "1.0  "
    for xpar,i in zip(parmat.xpars,range(len(parmat.xpars))):
        val = parmat.getElem(xpar,ypar)
        absval = abs(val)
        if val >= 0.0 : sign = " + "
        else: sign = " - "
        expr = expr + " {0} {1}*@{2}".format(sign,absval,i)

    parlist = ",".join(parmat.xpars)

    return "expr::{0}_new('{1}',{2})".format(ypar, expr, parlist)

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser("create multi-variate gaussian RooWorkspace")
    parser.add_argument('-i','--input',action='append',nargs="+",help="files with input information",required=True)
    parser.add_argument('-o','--output',help="output ROOT file",required=True)
    parser.add_argument( "--pois",nargs="+"     , type=str,     dest="pois"                 , help="list of POI names", default=[])
    parser.add_argument( "--workspace",           type=str,  dest="ws", help="name of output workspace"     , required=False,default="combWS")    
    parser.add_argument( "--expected",            action='store_true',  help="create an expected workspace"   , required=False)    
    parser.add_argument( "--data",                type=str,  help="name of dataset in RooFit workspace"   , required=False, default="combData")    
    parser.add_argument( "--rotation",            type=str,  help="name of dataset in RooFit workspace"   , required=False, default="")    

    args = parser.parse_args()
    make_combined_mvg(args)

