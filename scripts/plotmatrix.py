from RooFitUtils.util import formatNumberPDG
from senstiEFT.matrix import load_matrix, load_parameterisation
from RooFitUtils.pgfplotter import writematrix
from RooFitUtils.hepdatawriter import writematrix as writehepdatamatrix
from RooFitUtils.csvwriter import writematrix as writecsvmatrix
from senstiEFT.helpers import correlation_from_covariance
from os.path import join as pjoin
from numpy import array
from numpy.linalg import inv,pinv
import sys

def formatInterval(x):
    return "["+formatNumberPDG(x[0])+","+formatNumberPDG(x[1])+"]"

def convert(ev_names,parnames,evs):
    allvals = [ ]
    for ev in ev_names:
        d = { e[0]:e[1] for e in evs[ev] }
        vals = []
        for p in parnames:
            if p in d.keys():
                vals.append(d[p])
            else:
                vals.append(0.)
        allvals.append(vals)
    return allvals

rotmat = load_matrix(sys.argv[1])
#rotmat = load_parameterisation(sys.argv[1])
output = sys.argv[2]
norm = sys.argv[3]

if norm == "norm": rotmat.matrix = correlation_from_covariance(rotmat.matrix)

matlabels=["$\\sqrt{s}=$13 TeV, 36.1-139 fb$^{\\scriptsize{-1}}$"]

parnames = [x for x in rotmat.xpars]
ev_names = [y for y in rotmat.ypars]
ev_names.reverse()
mat = [[rotmat.getElem(x,y) for x in parnames] for y in ev_names]

writematrix("Internal",parnames,ev_names,
            mat,
            output,-1,1,45,plotlabels=matlabels)

