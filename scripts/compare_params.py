#!/bin/env python
from senstiEFT.matrix import load_parameterisation, matrix
from senstiEFT.senstivity_helpers import make_infomat
from senstiEFT.matrix_helpers import add_list_of_matrices, get_padded_matrix
import numpy as np
import math 
import ROOT

def compare_pars(pars1, pars2):
    for x in pars1:
        if x.upper() not in [y.upper() for y in pars2]:
           print("{0} is not common!".format(x))

def common_pars(pars1, pars2):
    return [ x for x in pars1 if x in pars2]

def compare_params(args):
    parmat1 = load_parameterisation(args.param1)
    parmat2 = load_parameterisation(args.param2)
#    print(parmat1.ypars)
#    print(parmat2.ypars)
    parmat1.xpars = [x.upper() for x in parmat1.xpars]
    parmat2.xpars = [x.upper() for x in parmat2.xpars]
    compare_pars(parmat1.ypars, parmat2.ypars)
    compare_pars(parmat1.xpars, parmat2.xpars)
    ypars = common_pars(parmat1.ypars, parmat2.ypars)
    xpars = common_pars(parmat1.xpars, parmat2.xpars)

    if args.save:
       parmat1_red = matrix("parmat1_reduced",[[ parmat1.getElem(x,y) for x in xpars] for y in ypars], xpars = xpars, ypars = ypars)
       parmat2_red = matrix("parmat2_reduced",[[ parmat2.getElem(x,y) for x in xpars] for y in ypars], xpars = xpars, ypars = ypars)
       diffname = "diff"
       diff = matrix(diffname,[[ parmat1.getElem(x,y) - parmat2.getElem(x,y) for x in xpars] for y in ypars], xpars = xpars, ypars = ypars)
       parmat1_red.save_matrix("{0}.json".format(parmat1_red.name))
       parmat2_red.save_matrix("{0}.json".format(parmat2_red.name))
       diff.save_matrix("{0}.json".format(diff.name))


    for ypar in sorted(ypars):
        str0  = ypar.ljust(42)+" : "        
        for xpar in sorted(xpars):
            val = parmat1.getElem(xpar, ypar) - parmat2.getElem(xpar, ypar) 
            if val < 0 : sign = " - "
            else: sign = " + "
         #   if args.fractional and val != 0.0: 
         #        val =100*val/ parmat1.getElem(xpar, ypar)
            if val != 0.0: str0 = str0 + "{0} {1:.3f} {2}".format(sign, abs(val), xpar).ljust(15)
        print(str0)


if __name__ == '__main__':   
    from argparse import ArgumentParser
    parser = ArgumentParser("create multi-variate gaussian RooWorkspace")
    parser.add_argument( "--param1"    ,            type=str,  dest="param1", help="name of output workspace"     , required=False,default="")    
    parser.add_argument( "--param2"    ,            type=str,  dest="param2", help="name of output workspace"     , required=False,default="")    
    parser.add_argument( "--save"      , action='store_true',  dest="save"  , help="save file"   , required=False,default=False)    
    parser.add_argument( "--fractional",  action='store_true',  dest="fractional", help="% difference "     , required=False,default=False)    

    args = parser.parse_args()
    compare_params(args)
